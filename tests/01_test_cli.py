"""
Copyright 2023, Georg Pfuetzenreuter

Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence").
You may not use this work except in compliance with the Licence.
An English copy of the Licence is shipped in a file called LICENSE along with this applications source code.
You may obtain copies of the Licence in any of the official languages at https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12.

---

Testing functions for Scullery - a SaltStack testing tool.
"""

import pytest
import os
import dotenv


def test_no_arguments(script_runner, script):
    result = script_runner.run(script)
    assert result.success is False
    assert 'the following arguments are required: --suite' in result.stderr


def test_no_config(script_runner, script):
    result = script_runner.run(script, '--suite', 'foo', print_result=True) 
    assert result.success is False
    assert 'Unable to locate configuration file' in result.stderr


@pytest.mark.parametrize('section,message', [
        ('box', 'No "box" section found in the configuration file'),
        ('suites', 'No suites configured'),
        ('boxes', 'No boxes configured'),
        ('suite_box', 'Specified suite does not reference a box'),
        ('box_name', 'Box configuration is incomplete'),
        ('box_image', 'Box configuration is incomplete'),
    ])
def test_config_incomplete(script_runner, script, testbase, section, message):
    configfile = '{}/configs/missing_{}.ini'.format(testbase, section)
    result = script_runner.run(script, '--config', configfile, '--suite', 'one_minion')
    assert result.success is False
    assert message in result.stderr


@pytest.mark.parametrize('section,message', [
        ('suite_box', 'Suite references an undefined box')
    ])
def test_config_undefined(script_runner, script, testbase, section, message):
    configfile = '{}/configs/undefined_{}.ini'.format(testbase, section)
    result = script_runner.run(script, '--config', configfile, '--suite', 'one_minion')
    assert result.success is False
    assert message in result.stderr


@pytest.mark.parametrize('config', ['complete'], indirect=True)
@pytest.mark.parametrize('suite,report', [
        ('one_minion', "[Status(name='scullery-minion0', state='not_created', provider='libvirt')]"),
        ('two_minions', "[Status(name='scullery-minion0', state='not_created', provider='libvirt'), Status(name='scullery-minion1', state='not_created', provider='libvirt')]"),
        ('one_master', "[Status(name='scullery-master0', state='not_created', provider='libvirt')]"),
        ('one_minion_one_master', "[Status(name='scullery-master0', state='not_created', provider='libvirt'), Status(name='scullery-minion0', state='not_created', provider='libvirt')]"),
        ('two_minions_one_master', "[Status(name='scullery-master0', state='not_created', provider='libvirt'), Status(name='scullery-minion0', state='not_created', provider='libvirt'), Status(name='scullery-minion1', state='not_created', provider='libvirt')]")
    ])
def test_status_report_not_running(script_runner, script, config, suite, report):
    result = script_runner.run(script, '--config', config, '--suite', suite, '--status', '--debug', '--env')
    assert result.success
    assert result.stderr.endswith("INFO - main_interactive: Status report: {}\n".format(report))


@pytest.mark.parametrize('config', ['complete'], indirect=True)
@pytest.mark.parametrize('suite', [
        'one_minion',
        'two_minions',
        'one_master',
        'one_minion_one_master',
        'two_minions_one_master'
    ])
def test_launch_stop(script_runner, script, virt, config, suite):
    cmd = (script, '--config', config, '--suite', suite)
    result = script_runner.run(*cmd)
    assert result.success
    assert 'Launching {} ...\n'.format(suite) in result.stderr
    domains = []
    print(virt.getURI())
    print(virt.listDomainsID())
    for domain in virt.listDomainsID():
        print(domain)
        domains.append(virt.lookupByID(domain).name())
    print(str(domains))
    # consider refining this
    assert any('scullery' in domain for domain in domains)
    result = script_runner.run(*cmd, '--stop')
    assert result.success
    assert 'Destroying machines ...\n' in result.stderr
    domains = []
    for domain in virt.listDomainsID():
        domains.append(virt.lookupByID(domain).name())
    assert not any('scullery' in domain for domain in domains)


@pytest.mark.parametrize('config', ['complete'], indirect=True)
@pytest.mark.parametrize('suite,masters,minions', [
        ('one_minion', None, 'scullery-minion0'),
        ('two_minions', None, 'scullery-minion0,scullery-minion1'),
        ('one_master', 'scullery-master0', None),
        ('one_minion_one_master', 'scullery-master0', 'scullery-minion0'),
        ('two_minions_one_master', 'scullery-master0', 'scullery-minion0,scullery-minion1')
    ])
def test_envfile(script_runner, script, config, suite, masters, minions):
    cmd = (script, '--config', config, '--suite', suite)
    envfile = '.scullery_env'
    result = script_runner.run(*cmd, '--env')
    assert result.success
    assert os.path.isfile(envfile)
    envmap = dotenv.dotenv_values(envfile)
    if masters is not None:
        assert envmap['SCULLERY_MASTERS'] == masters 
    if minions is not None:
        assert envmap['SCULLERY_MINIONS'] == minions
    script_runner.run(*cmd, '--stop')
    assert os.path.isfile(envfile) is False


@pytest.mark.parametrize('config', ['complete'], indirect=True)
def test_test_undeclared(script_runner, script, config):
    result = script_runner.run(script, '--config', config, '--suite', 'one_minion', '--test')
    assert not result.success
    assert result.stderr.endswith('Tests requested but not declared in suite configuration\n')


@pytest.mark.parametrize('config', ['complete'], indirect=True)
def test_test_undefined(script_runner, script, config):
    result = script_runner.run(script, '--config', config, '--suite', 'one_minion_bogus_test', '--test')
    assert not result.success
    assert result.stderr.endswith('Specified test is not defined\n')


@pytest.mark.parametrize('config', ['complete'], indirect=True)
def test_test_incomplete(script_runner, script, config):
    result = script_runner.run(script, '--config', config, '--suite', 'one_minion_bogus_test_2', '--test')
    assert not result.success
    assert result.stderr.endswith('Incomplete test configuration\n')


@pytest.mark.parametrize('config', ['complete'], indirect=True)
@pytest.mark.parametrize('suite', [
        'one_minion',
        'two_minions',
        'one_master',
        'one_minion_one_master',
        'two_minions_one_master',
        'one_minion_bootstrap',
        'one_minion_one_master_bootstrap',
        'one_minion_salt',
        'two_minions_one_master_salt'
    ])
def test_stop(script_runner, script, config, suite):
    cmd = (script, '--config', config, '--suite', suite, '--stop', '--force')
    result = script_runner.run(*cmd)
    assert result.success


def test_stop_success(virt):
    domains = []
    for domain in virt.listDomainsID():
        domains.append(virt.lookupByID(domain).name())
    assert not any('scullery' in domain for domain in domains)


@pytest.mark.parametrize('config', ['complete'], indirect=True)
@pytest.mark.parametrize('suite', [
        'one_minion',
        'two_minions',
        'one_master',
        'one_minion_one_master',
        'two_minions_one_master'
    ])
def test_already_stopped(script_runner, script, virt, config, suite):
    cmd = (script, '--config', config, '--suite', suite, '--stop', '--debug')
    result = script_runner.run(*cmd)
    assert result.success
    assert result.stderr.endswith('DEBUG - main_interactive: Deployment is not running\n')
