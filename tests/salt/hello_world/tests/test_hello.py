def test_hello_world_content(host):
    assert host.file('/srv/hello_world.txt').content.decode('UTF-8') == 'Hello salted world!\n'
