"""
Copyright 2023, Georg Pfuetzenreuter

Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence").
You may not use this work except in compliance with the Licence.
An English copy of the Licence is shipped in a file called LICENSE along with this applications source code.
You may obtain copies of the Licence in any of the official languages at https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12.

---

Testing functions for Scullery - a SaltStack testing tool.
"""

import pytest
import os
from conftest import loadenv
import sys

@pytest.mark.parametrize('config', ['complete'], indirect=True)
@pytest.mark.parametrize('suite', [
        'one_minion_bootstrap',
        'one_minion_one_master_bootstrap',
    ])
def test_bootstrap(script_runner, script, config, suite, vag, testbase):
    bootstrap_script = '{}/scripts/bootstrap_hello_world.txt'.format(testbase)
    do_digest = False
    if sys.version_info[1] > 10:
        do_digest = True
        import hashlib
        with open(bootstrap_script, 'rb') as fh:
                  digest_local = hashlib.file_digest(fh, 'md5')
    cmd = (script, '--config', config, '--suite', suite)
    result = script_runner.run(*cmd, '--env')
    assert result.success
    assert result.stderr.endswith('main_interactive: Launching {} ...\n'.format(suite))
    v = vag
    v.env = loadenv()
    assert v.ssh(command='cat /srv/hello_world.txt') == 'Hello world!\n'
    if do_digest:
        digest_remote = v.ssh(command='md5sum /srv/hello_world.txt | awk "{ print $1 }"')
        assert digest_local == digest_remote
    assert script_runner.run(*cmd, '--stop')

