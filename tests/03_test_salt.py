"""
Copyright 2023, Georg Pfuetzenreuter

Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence").
You may not use this work except in compliance with the Licence.
An English copy of the Licence is shipped in a file called LICENSE along with this applications source code.
You may obtain copies of the Licence in any of the official languages at https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12.

---

Testing functions for Scullery - a SaltStack testing tool.
"""

import pytest

@pytest.mark.parametrize('config', ['complete'], indirect=True)
@pytest.mark.parametrize('suite,count', [
        ('one_minion_salt', 0),
        ('two_minions_one_master_salt', 1)
    ])
def test_salt_test(script_runner, script, config, suite, count):
    cmd = (script, '--config', config, '--suite', suite)
    result = script_runner.run(*cmd, '--debug', '--env', '--test')
    assert result.success
    for message in [
            'File /srv/hello_world.txt',
            'Succeeded: 1',
            'DEBUG - main_interactive: state.apply requested',
            'INFO - main_interactive: Initiating tests ...',
            'DEBUG - runtests: Test result is 0',
            'DEBUG - runtests: Test succeeded'
        ]:
        assert message in result.stderr
    assert not any(term in result.stderr for term in [
            'FAILED', 'WARNING - runtests: Tests failed',
            'DEBUG - runtests: Test result is 1', 'AssertionError'
        ])
    if count > 0:
        minions = [0, 1]
    else:
        minions = [0]
    for m in minions:
        assert 'test_hello_world_content[paramiko://scullery-minion%i] PASSED' % m in result.stdout
    assert script_runner.run(*cmd, '--stop')
