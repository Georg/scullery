"""
Copyright 2023, Georg Pfuetzenreuter

Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence").
You may not use this work except in compliance with the Licence.
An English copy of the Licence is shipped in a file called LICENSE along with this applications source code.
You may obtain copies of the Licence in any of the official languages at https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12.

---

Testing functions for Scullery - a SaltStack testing tool.
"""

import pytest
import os
import libvirt
import vagrant
import dotenv

@pytest.fixture
def script():
    is_packaged = False
    script = 'scullery'
    if not is_packaged:
        script = f'{script}.py'
    return script


@pytest.fixture
def testbase():
    return os.path.abspath(os.path.dirname(os.getenv('PYTEST_CURRENT_TEST')))


@pytest.fixture
def config(testbase, request):
    return '{}/configs/{}.ini'.format(testbase, request.param)


@pytest.fixture
def virt():
    return libvirt.openReadOnly('qemu:///system')

@pytest.fixture
def vag():
    return vagrant.Vagrant(quiet_stderr=False, quiet_stdout=False)

def loadenv():
    env = os.environ.copy()
    envmap = dotenv.dotenv_values('.scullery_env')
    for variable, value in envmap.items():
        if value is not None:
            if isinstance(value, list):
                value = ','.join(value)
            env[variable] = value
    return env
