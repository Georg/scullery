testbase='/vagrant/tests/salt/hello_world'
sls='hello_world.sls'

ln -s "$testbase/states/$sls" /srv/salt/
ln -s "$testbase/pillar/$sls" /srv/pillar/

tee /srv/pillar/top.sls >/dev/null <<EOF
base:
  '*':
    - hello_world
EOF

if systemctl is-enabled salt-master
then
	count=0
	until salt -t10 scullery-\* test.ping
	do
		if [ "$count" = 3 ]
		then
			logger -s 'No minions connected'
			break
		fi
		echo 'Waiting for minions to connect ...'
		sleep 5
		count=$((count+1))
	done
fi
