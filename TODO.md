# To-Do's

- Introduce `--apply` argument to execute `state.apply` on VM's without chaining to test executions
- Do not restart the network stack on `--refresh` (the `systemctl restart network` call is needed on the first boot to get Avahi DNS resolution to work - there is currently no logic to skip the command on subsequent executions of the bootstrap script)
- Add example configurations
- Use `pyproject.toml` style packaging to allow for installation as a Python module
- Resolve Pytest warnings
