# Scullery

This is intended to be a replacement for [KitchenCI](https://test-kitchen.chef.io/) and [kitchen-salt](https://github.com/saltstack/kitchen-salt), focussing on testing SaltStack states using Vagrant and Pytest/Testinfra.

## Installation

So far, a packaged installation is possible on openSUSE Tumbleweed:

```
$ zypper ar -f https://download.opensuse.org/repositories/home:/crameleon:/Scullery/openSUSE_Tumbleweed/home:crameleon:Scullery.repo
$ zypper in scullery
```

This will pull in `vagrant`, `vagrant-libvirt`, `python3-vagrant`, `python3-pytest` and `python3-pytest-testinfra` as dependencies.

Installation on Leap 15.4 is possible, however will currently cause dependency issues with the needed `python-vagrant` library.

It is possible to run `scullery.py` from a Git checkout of this repository if the dependencies are resolved manually.

Currently the script behaves purely as a console application and imports as a Python module are not supported.

## Basic usage

```
scullery -h
usage: scullery [-h] [--debug] [--config CONFIG] [--env] --suite SUITE [--stop | --test | --status | --refresh] [--force-stop]

options:
  -h, --help       show this help message and exit
  --debug          Print extremely verbose output
  --config CONFIG  Specify the configuration file to use
  --env            Write environment file for direct use of Vagrant
  --suite SUITE    Specify the suite to run
  --stop           Stop running machines
  --test           Start machines and run tests
  --status         Get Vagrant deployment status
  --refresh        Re-sync files and re-run bootstrap scripts
  --force-stop     Invoke Vagrant destruction without having detected any running VM's
```

## Command line examples

* To run a complete test suite:  
  `$ scullery --suite <name of suite> --test`

* To create Vagrant VM's without invoking the tests:
  `$ scullery --suite <name of suite>`

* To refresh the VM's Salt files:  
  `$ scullery --suite <name of suite> --refresh`

* To stop the VM's and tidy up:  
  `$ scullery --suite <name of suite> --stop`

## Interacting with Vagrant

It is possible to directly interact with the Vagrant environment templated by Scullery. To do so, generate an environment file by adding `--env` to any command line call - for example:

`$ scullery --suite <name of suite> --env`

This will write a `.scullery_env` file to the current working directory. Export the environment variables in the file using your Shell, afterwards use `vagrant` like you normally would. Example for `bash`/`sh`:

```
$ set -a; . .scullery_env; set +a
$ vagrant ssh # or any other Vagrant command
```

Do not forget to unset the exported variables before you call Scullery with a different `--suite`!

## Hacking

To test Scullery itself, call Pytest with your favourite arguments from the repository root:

```
$ pytest -v -rx -x tests/*.py
```

Make sure to not have _any_ other Scullery based Vagrant VM's running in the same Libvirt session and no `SCULLERY_*` or `VAGRANT_*` variables exported in your shell.

The complete test suite takes about 30 minutes to execute on an Intel i7-6820HQ CPU with SSHD storage. If you want to test only certain functionality, simply add `-k <name of test function>`.

## License

This project is licensed under the European Union Public License found in the file `LICENSE`.
